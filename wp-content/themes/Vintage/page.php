<?php get_header();?>

<?php
/*
        <h1><?php the_title();?></h1>
        <?php if (have_posts()) : while(have_posts()) : the_post();?>
            <?php the_content();?>
        <?php endwhile; endif;?>
*/
?>    
<div class="catalogue-bg">
            <div class="catalogue-title">
                <h1>NTIBANI</h1>
                <h2 class="coctails-title text-pink">Coctails</h2>
            </div>
            <!-- Slider main container -->
            <div class="swiper-container">
            <!-- Additional required wrapper -->
            <div class="swiper-wrapper">
            <!-- Slides -->
            <div class="swiper-slide">
                <div class="swiper-item">
                    <div class="d-flex">
                        <h2 class="coctails-title">Blooming Althea</h2>
                        <h2 class="coctails-title price">8€</h2>
                    </div>
                    <p class="coctails-description">
                        Τον 16 αιώνα υπηρχε η αντίληψη πως το τονωτικό νερό σε συνδιασμο
                        με το <b>gin</b> είχαν θεραπευτικές ιδιότητες. Εμείς ανθίσαμε το αγαπημένο 
                        σας «φάρμακο» με <b>ιβίσκο</b>, αρωματισμένο με bitter απο δαμάσκηνο και
                        δροσίσαμε με dry tonic. Στην υγειά σας!
                    </p>
                </div>
                <img class="devider text-black" src="./assets/images/divider/divider-5318234_640.png" alt="">
                <div class="swiper-item">
                    <div class="d-flex">
                        <h2 class="coctails-title">La Pina Escapo</h2>
                        <h2 class="coctails-title price">8€</h2>
                    </div>
                    <p class="coctails-description">
                        Ο "σουρωμένος ανανάς" δραπέτευσε και πήρε μαζι του <b>ρούμι</b>, νότες
                        απο κακάο, γάλα καρύδας, αρωματα κανέλας-σοκολατας και tiki bitters.
                    </p>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="swiper-item">
                    <div class="d-flex">
                        <h2 class="coctails-title">" Kati me Gin ..."</h2>
                        <h2 class="coctails-title price">8€</h2>
                    </div>
                    <p class="coctails-description">
                        Δεν ειναι λιγες οι φορές που μας ζητήσατε "κάτι με gin". Έτσι εμείς
                        μετατρέψαμε την επιθυμία σας σε συνταγή. Gin, δροσερό καρπούζι, 
                        πικραμύγδαλο και κόκκινο vermouth απο μαύρα μούρα συνθέτουν την πιο καλοκαιρινή εκδοχή
                        του "Κάτι με Gin..."
                    </p>
                </div>
                <img class="devider text-black" src="./assets/images/divider/divider-5318234_640.png" alt="">
                <div class="swiper-item">
                    <div class="d-flex">
                        <h2 class="coctails-title">Jane from the Jungle</h2>
                        <h2 class="coctails-title price">9€</h2>
                    </div>
                    <p class="coctails-description">
                        To coctail που απολάμβανε η Jane όταν o Tarzan γονάτισε και της έκανε πρόταση γάμου.
                        Απο τότε έγινε το αγαπημένο της, <b>Blend παλαιών ρουμιών</b>, φρέσκος χυμός ανανά,
                        κανέλα, λικέρ μπανάνας και βανίλια βουτύρου, συνθέτουν μια μοναδική στιγμή.
                    </p>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="swiper-item">
                    <img class="devider text-black" src="./assets/images/divider/divider-5318234_640.png" alt="">
                    <p class="quote"> <q>
                        Το αλκοόλ μπορεί να είναι ο μεγαλύτερος εχθρος του ανθρώπου,
                        αλλά η Βίβλος λέει να αγαπάς τον εχθρό σου.

                    </q> - Frank Sinatra</p>
                    <div class="btn-container">
                        <a class="waves-effect waves-light btn" href="https://www.facebook.com/ntivani/" ><i class="fas fa-wifi left"></i>Προσβαση στο wifi</a>
                    </div>
                </div>
            </div>
            </div>
            <!-- If we need navigation buttons -->
            <div class="swiper-button-prev-custom"><img class="arrow-img" src="<?php echo get_template_directory_uri(); ?>/assets/images/divider/arrow_left.png" alt=""></div>
            <div class="swiper-button-next-custom"><img class="arrow-img" src="<?php echo get_template_directory_uri(); ?>/assets/images/divider/arrow_right.png" alt=""></div>

            </div>
        </div>

<?php get_footer();?>