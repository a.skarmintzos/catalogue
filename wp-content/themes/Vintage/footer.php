<?php wp_footer();?>

</body>
<script type="module">
            import Swiper from '<?php echo get_template_directory_uri(); ?>/assets/js/swiper-bundle.esm.browser.min.js'
          
            var swiper = new Swiper('.swiper-container', {
            scrollbar:{
                hide: true
                },
            navigation: {
                nextEl: '.swiper-button-next-custom',
                prevEl: '.swiper-button-prev-custom',
            },
            }); 
        </script>
</html>