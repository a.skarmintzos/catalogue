<?php

// Custom Favicon
function add_favicon() {
	echo '<link rel="shortcut icon" type="image/png" href="'.get_template_directory_uri().'/assets/images/favicon/favicon-32x32.png" />';
        }

        add_action('wp_head', 'add_favicon');

function load_stylesheets()
{
        // wp_register_style('bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css', 
        // array(), false, 'all');
        // wp_enqueue_style('bootstrap');
        wp_register_style('style-font-awesome',  'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css', 
        array(), false, 'all');
        wp_enqueue_style('style-font-awesome');
        wp_register_style('style-swiper',  get_template_directory_uri() . '/assets/css/swiper.min.css', 
        array(), false, 'all');
        wp_enqueue_style('style-swiper');
        wp_register_style('style-materialize', get_template_directory_uri() . '/assets/sass/materialize.css', 
        array(), false, 'all');
        wp_enqueue_style('style-materialize');
       
        wp_register_style('style-main', get_template_directory_uri() . '/assets/css/style-main.css', 
        array(), false, 'all');
        wp_enqueue_style('style-main');
       
}
add_action('wp_enqueue_scripts', 'load_stylesheets');


function loadjs()
{
        wp_register_script('materialize', get_template_directory_uri() . '/assets/js/materialize.min.js', '', 1, true);
        wp_enqueue_script('materialize');
        wp_register_script('swiperjs', get_template_directory_uri() . '/assets/js/swiper.bundle.js', '', 1, false);
        wp_enqueue_script('swiperjs');
        wp_register_script('customjs', get_template_directory_uri() . '/assets/js/scripts.js', '', 1, true);
        wp_enqueue_script('customjs');
        
}
add_action('wp_enqueue_scripts', 'loadjs');


add_image_size('smallets', '300', '300', true);
add_image_size('bigger', '600', '600', true);

// Theme Options
add_theme_support('custom-background');
add_theme_support('post-thumbnails');


//Custom Image Sizes
add_image_size('blog-large', 800, 400, true);


// //Custom Page Type for Menu Items
function menu_post_type()
{
         $args = array(
                'labels' => array(

                        'name' => 'Menu Items',
                        'singular_name' => 'Menu Item',
                ),
                'description' => 'This is the whole menu',
                'hierarchical' => false, //Makes Whole section act more like a "Page". On false acts more like a "Post"
                'public' => true,
                'has_arcive' => false,
                'menu_icon' => 'dashicons-food',
                'supports' => array('title', 'editor', 'custom-fields','thumbnail')
        );
        register_post_type('menu', $args);
}
add_action('init', 'menu_post_type');

function menu_sections_taxonomy()
{
        $args = array(
                'labels' => array(
                        'name' => 'Categories',
                        'singular_name' => 'Category',
                ),
                'public' => true,
                'hierarchical' => false,
                'supports' => array('title', 'custom-fields','thumbnail')
        );
        register_taxonomy('Coctails', array('menu'), $args);
}
add_action('init', 'menu_sections_taxonomy');

// function menu_taxonomy()
// {
//         $args = array(
//                 'labels' => array(

//                         'name' => 'Μeals',
//                         'singular_name' => 'Coctail',
//                 ),
//                 'public' => true,
//                 'hierarchical' => true //On "true" content acm more like a category. On "false" act more like
//         );
// }
// add_action( 'init', 'menu_taxonomy');