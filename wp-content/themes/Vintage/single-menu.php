
<?php get_header(); ?>
<div class="container">
    <div class="row">
        <div class="col-6">
            <?php if(has_post_thumbnail( )): ?>
                <img src="<?php the_post_thumbnail_url('blog-large');?>" alt="<?php the_title();?>">
            <?php endif;?>
        </div>
        <div class="col-6">
            <?php the_content(); ?>
            <?php get_template_part(); ?>
        </div>
        
    </div>
    
</div>